//var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var http = require('http').Server(app);

var io = require('socket.io')(http);

var port = process.env.PORT || 3000;
var data =  [
    {
        id: 1,
        name: 'Mohit Jain --1'
    },
    {
        id: 2,
        name: 'Taroon Tyagi'
    },
    {
        id: 3,
        name: 'Rahul Narang'
    }
];

app.use(bodyParser.json());

app.get('/phone', function(req,res){
    res.status(200).send(data);
});

app.put('/phone/:id', function(req,res){
    console.log(req.body);

    data.forEach(function(element) {
        if(element.id === req.body.id) {
            element.name = req.body.name;
        }
    });

    res.status(200).send('ok');
});

app.use(express.static(__dirname + '/'));

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('chat message', function(msg){
        console.log('message: ' + msg);

        io.emit('chat message', msg);
    });

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});


http.listen(port, function(){
    console.log('Server start success = ' + port);
});
