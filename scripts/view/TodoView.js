define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/todos.html',
    'socket'
], function ($, _, Backbone, oneTemplate, io) {
    'use strict';

    var TodoView = Backbone.View.extend({

        tagName:  'div',

        template: _.template(oneTemplate),

        events: {

            "click .add"   : "closeSave",
            "click .dell"  : "dell"
        },

        initialize: function () {
            this.socket = io();
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        },

        closeSave: function() {
            var val = this.$('.replace').val();

            this.$('.new-todo').hide();
            this.$('.toggle').show();

            val && this.model.save({name: val});

            this.model.trigger('showButton');

            this.socket.emit('chat message', 'Alex');
        },

        dell: function() {
            this.model.destroy();
            this.socket.emit('chat message', 'Alex');
        }
    });

    return TodoView;
});