define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/page.html',
    'TodoView'
], function ($, _, Backbone, pageTemplate, TodoView) {
    'use strict';

    var PageView = Backbone.View.extend({

        tagName:  'li',

        template: _.template(pageTemplate),

        events: {
            "click .edit": "open"
        },

        initialize: function () {
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'showButton', this.render);
        },

        render: function () {
            var view = new TodoView({ model: this.model });

            this.$el.html(this.template(this.model.toJSON()));
            this.$el.append(view.render().el);

            return this;
        },

        open: function(e) {
            this.$('.new-todo').show();
            this.$('.toggle').hide();
            this.$('.edit').hide();
        },

        showButton: function() {
            this.$('.edit').show();
        }
    });

    return PageView;
});