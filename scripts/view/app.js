define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/stats.html',
    'PageView',
    'collection/collection',
    'socket',
    'utils'
], function ($, _, Backbone, statsTemplate, PageView, ItemCollection, io, utils) {
    'use strict';

    var AppView = Backbone.View.extend({

        el: '#todoapp',

        events: {
            'click #ok': 'go'
        },

        template: _.template(statsTemplate),

        initialize: function () {
            this.socket = io();
            this.$hi = this.$('#hi');
            this.$list = this.$('#list');

            this.listenTo(ItemCollection, 'add', this.addOne);
            this.listenTo(ItemCollection, 'change', this.renderAll, this);

            this.socket.on('chat message', function(msg){
                this.addAll();
            }.bind(this));

            this.render();


            utils.sayHello('Alex').sayGoodbye();
        },

        render: function () {
            this.$hi.html(this.template({
                remaining: 'Hi!'
            }));

            this.addAll();
        },

        go: function() {
            var val = this.$('.addNewUser');

            if(val.val()) {
                ItemCollection.add({name: val.val()});
                this.socket.emit('chat message', 'Alex');
            }

            val.val('');
        },

        addOne: function (todo) {
            var view = new PageView({ model: todo });

            this.$list.append(view.render().el);
        },

        addAll: function () {
            ItemCollection.fetch({remove: true});
        },

        renderAll: function() {
            this.$list.empty();
            ItemCollection.each(this.addOne, this);
        }
    });

    return AppView;
});