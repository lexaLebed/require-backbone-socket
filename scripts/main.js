requirejs(['utils', 'underscore', 'backbone', 'jquery', 'AppView'], function(utils, _, Backbone, $, AppView) {
    utils.sayHello();

    new AppView();
});