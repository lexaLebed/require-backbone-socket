requirejs.config({
    baseUrl: './scripts',
    deps: ['main'],
    paths: {
        backbone: '../bower_components/backbone/backbone-min',
        underscore: '../bower_components/underscore/underscore-min',
        jquery: '../bower_components/jquery/dist/jquery.min',
        text: '../bower_components/text/text',
        utils: 'helper/utils',
        AppView: 'view/app',
        TodoView: 'view/TodoView',
        PageView: 'view/pageView',
        socket: '/socket.io/socket.io.js'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        }
    }
});