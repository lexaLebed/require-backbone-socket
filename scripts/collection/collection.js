define([
    'underscore',
    'backbone',
    'model/model'
], function (_, Backbone, Store, itemModel) {
    'use strict';

    var ItemCollection = Backbone.Collection.extend({
        models: itemModel,
        url: '/phone'
    });

    return new ItemCollection();
});