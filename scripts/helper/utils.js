define(function(){
    return {
        name: 'Defaul name',
        sayHello: function(name) {
            this.name = name || this.name
            console.log( 'Hi there, ' + this.name );

            return this;
        },
        sayGoodbye: function(name) {
            console.log( 'Goodbye, ' + this.name );

            return this;
        }
    }
});


var _$ = (function(){

    var defaultName = 'Defaul name Alex';

    function sayHello(name) {
        defaultName = name || defaultName;

        console.log( 'Hi there, ' + defaultName );

        return this;
    }

    function sayGoodbye() {
        console.log( 'Goodbye, ' + defaultName );

        return this;
    }

    function find(arr, item) {
        var array = arr || [];

        for (var i = 0; i < array.length; i++) {
            if (arr[i] === item) {
                return i;
            }
        }

        return null;
    }

    function search(arr, funct) {
        var array = arr || [];
        var result;

        for (var i = 0; i < array.length; i++) {
            result = funct(array[i]) || result;
        }

        return result;
    }

    function searchAll(arr, funct) {
        var array = arr || [];
        var result = [];

        for (var i = 0; i < array.length; i++) {
            funct(array[i]) && result.push(funct(array[i]));
        }

        return result;
    }

    return {
        sayHello: sayHello,
        sayGoodbye: sayGoodbye,
        find: find,
        search: search,
        searchAll: searchAll
    }
})();
